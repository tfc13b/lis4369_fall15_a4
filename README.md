##Timothy Carver
##Lis4369
1. Backward-engineer (using Visual Studio Community Edition) the following console
application screenshot:
2. Requirements (see below screenshots):
a. Display short assignment requirements.
b. Display *your* name as “author.”
c. Display current date/time (must include date/time, your format preference).
d. Create two classes: person and student (see fields and methods below).
e. Must include data validation on numeric data.
# Inheritance 
![A4.jpg](https://bitbucket.org/repo/b47jAo/images/4285586045-A4.jpg)
![A4_2.jpg](https://bitbucket.org/repo/b47jAo/images/193740114-A4_2.jpg)